/*
 * si_units.h
 *
 *  Created on: 6.2.2016
 *      Author: mistr
 */

#ifndef SI_UNITS_H_
#define SI_UNITS_H_

namespace si {

typedef double real_number;

class quantity;
class length;

class scalar
{
public:
    explicit scalar(real_number value):value_(value) { }
    scalar(const scalar &src):value_(src.value_) { }
    real_number get_value()const { return value_; }
private:
    real_number value_;
};

class length
{
public:
    length(const length &src):value_(src.value_) { }
    length& operator=(const length &src) { value_ = src.value_; return *this; }
    length operator/(scalar num)const { return length(value_ / num.get_value()); }
    real_number get_value()const { return value_; }
private:
    explicit length(real_number value):value_(value) { }
    real_number value_;
    friend class quantity;
};

class quantity
{
public:
    explicit quantity(real_number value):value_(value) { }
    quantity kilo()const { return quantity(1000 * value_); }
    quantity mili()const { return quantity(value_ / 1000); }
    length meter()const { return length(value_); }
private:
    real_number value_;
};

}//namespace si

#endif//SI_UNITS_H_
