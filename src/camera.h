/*
 * camera.h
 *
 *  Created on: 6.2.2016
 *      Author: mistr
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#include "si_units.h"

namespace raytrc {

class camera
{
public:
    typedef si::length length;
    typedef si::scalar scalar;
    camera();
    ~camera();
    camera& set_focal_length(length value);
    camera& set_f_number(scalar value);
    camera& set_pupil_diameter(length value);
    camera& set_focus_length(length value);
private:
    static length calculate_pupil_diameter(scalar f_number, length focal_length);
    length focal_length_;
    length pupil_diameter_;
    length focus_length_;
};

}//namespace raytrc

#endif//CAMERA_H_
