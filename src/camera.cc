/*
 * camera.cc
 *
 *  Created on: 6.2.2016
 *      Author: mistr
 */

#include "camera.h"

namespace raytrc {

camera::camera()
:   focal_length_(si::quantity(14).mili().meter()),
    pupil_diameter_(calculate_pupil_diameter(scalar(3.5), focal_length_)),
    focus_length_(focal_length_)
{
}

camera::~camera()
{
}

camera& camera::set_focal_length(length value)
{
    focal_length_ = value;
    return *this;
}

camera& camera::set_f_number(scalar f_number)
{
    pupil_diameter_ = calculate_pupil_diameter(f_number, focal_length_);
    return *this;
}

camera& camera::set_pupil_diameter(length value)
{
    pupil_diameter_ = value;
    return *this;
}

camera& camera::set_focus_length(length value)
{
    focus_length_ = value;
    return *this;
}

si::length camera::calculate_pupil_diameter(scalar f_number, length focal_length)
{
    return focal_length / f_number;
}

}//namespace raytrc

#if 0
           f                 f
x = l * -------; y = -h * -------
         l - f             l - f

        d * f * f * fn
|H| = -------------------; // L = l * (1 + d)
       (1 + d) * (l - f)
#endif
